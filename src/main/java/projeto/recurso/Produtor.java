package projeto.recurso;

import java.util.Collection;

public interface Produtor<T,R> {

    void produzir(R t);

    T adicionarConsumidor(Collection<Consumidor<R>> consumidores);

    T adicionarConsumidor(Consumidor<R> consumidor);

}
