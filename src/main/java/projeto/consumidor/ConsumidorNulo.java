package projeto.consumidor;

import projeto.modelo.Objeto;
import projeto.recurso.Consumidor;

public class ConsumidorNulo implements Consumidor<Objeto> {

    private static Consumidor<Objeto> INSTANCIA;

    private ConsumidorNulo() {}

    public static Consumidor<Objeto> obterInstancia(){
        if(INSTANCIA == null){
            INSTANCIA = new ConsumidorNulo();
        }
        return INSTANCIA;
    }

    @Override
    public void consumir(Objeto objeto) { }

}
