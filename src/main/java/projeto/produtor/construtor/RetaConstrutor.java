package projeto.produtor.construtor;

import projeto.modelo.Cor;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.modelo.Reta;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class RetaConstrutor extends Construtor {

    private Ponto a;
    private Ponto b;
    private IFigura figura;
    private Cor cor;

    @Override
    protected boolean objetoCompleto() {
        return a != null && b != null;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        Reta reta = new Reta(a, b, cor);
        figura.inserirReta(reta);
        return reta;
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        if (a == null) {
            a = ponto;
        } else {
            b = ponto;
        }
        this.cor = ponto.getCor();
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
        this.figura = figura;
    }

    @Override
    protected Construtor renovarInstancia() {
        return new RetaConstrutor();
    }

}
