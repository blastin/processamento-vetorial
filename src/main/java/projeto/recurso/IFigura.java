package projeto.recurso;

import projeto.modelo.*;

public interface IFigura {

    void inserirCirculo(Circulo circulo);

    void inserirReta(Reta reta);

    void inserirTriangulo(Triangulo triangulo);

    void inserirRetangulo(Retangulo retangulo);

    void inserirPoligono(Poligono poligono);

    void retornarEstadoAnterior();

    void limparFigura();

}
