package projeto.recurso;


public interface EntradaSaida<T> {

    void salvarObjeto(T T);

    T obterObjeto();

    EntradaSaida<T> adicionarSerializacao(Serializar<T> serializar);

    EntradaSaida<T> adicionarDeserializacao(Deserializar<T> deserializar);

}
