package projeto.utilidade;

import javafx.scene.canvas.Canvas;
import projeto.modelo.Ponto;

import java.util.function.Function;

public class EspelhamentoTela implements Function<Ponto, Ponto> {

    private final Canvas canvasDesenho;
    private final Double resolucaoEixoX;
    private final Double resolucaoEixoY;

    public EspelhamentoTela(Canvas canvasDesenho, Canvas canvasEspelho) {
        resolucaoEixoX = canvasEspelho.getWidth();
        resolucaoEixoY = canvasEspelho.getHeight();
        this.canvasDesenho = canvasDesenho;
    }

    @Override
    public Ponto apply(Ponto ponto) {
        return new
                Ponto
                (calculoParaEixoX(ponto) * resolucaoEixoX,
                        calculoParaEixoY(ponto) * resolucaoEixoY,
                        ponto.getCor()
                );
    }

    private Double calculoParaEixoX(Ponto ponto) {
        return ponto.getX() / canvasDesenho.getWidth();
    }

    private Double calculoParaEixoY(Ponto ponto) {
        return ponto.getY() / canvasDesenho.getHeight();
    }
}
