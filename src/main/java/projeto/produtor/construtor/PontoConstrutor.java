package projeto.produtor.construtor;

import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class PontoConstrutor extends Construtor {

    private Ponto ponto;

    @Override
    protected boolean objetoCompleto() {
        return ponto != null;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        return ponto;
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        this.ponto = ponto;
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
    }

    @Override
    protected Construtor renovarInstancia() {
        return new PontoConstrutor();
    }

}
