package projeto.produtor.construtor;

import projeto.modelo.Cor;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.modelo.Retangulo;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class RetanguloConstrutor extends Construtor {

    private Ponto a;
    private Ponto b;
    private IFigura figura;
    private Cor cor;

    @Override
    protected boolean objetoCompleto() {
        return a != null && b != null;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        Retangulo retangulo = new Retangulo(a, b, cor);
        figura.inserirRetangulo(retangulo);
        return retangulo;
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        if (a == null) {
            a = ponto;
        } else {
            b = ponto;
        }
        this.cor = ponto.getCor();
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
        this.figura = figura;
    }

    @Override
    protected Construtor renovarInstancia() {
        return new RetanguloConstrutor();
    }

}
