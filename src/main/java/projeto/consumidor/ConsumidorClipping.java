package projeto.consumidor;

import javafx.scene.canvas.Canvas;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.modelo.Retangulo;
import projeto.recurso.Consumidor;
import projeto.utilidade.EspelhamentoTela;
import projeto.utilidade.Grafico;

import java.util.List;

public class ConsumidorClipping implements Consumidor<Objeto> {

    private final Canvas canvas;
    private final Retangulo retangulo;

    private final Grafico grafico;

    private final EspelhamentoTela espelhamentoTela;

    public ConsumidorClipping(Canvas canvasSelecionado, Canvas canvasDesenho, Retangulo retangulo) {
        this.canvas = canvasSelecionado;
        this.retangulo = retangulo;
        grafico = Grafico.obterInstancia();
        espelhamentoTela = new EspelhamentoTela(canvasDesenho, canvasSelecionado);
    }

    @Override
    public void consumir(Objeto objeto) {

        List<Ponto> pontos = objeto.construir();

        pontos
                .stream()
                .filter(retangulo::pontoInterno)
                .map(espelhamentoTela)
                .forEach(ponto -> grafico.desenharPontoEmCanvas(canvas, ponto));

    }

}
