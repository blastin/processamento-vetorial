package projeto;

import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ColorPicker;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import projeto.consumidor.ConsumidorClipping;
import projeto.consumidor.ConsumidorDesenho;
import projeto.consumidor.ConsumidorEspelho;
import projeto.modelo.*;
import projeto.produtor.Construtor;
import projeto.produtor.FiguraProdutor;
import projeto.produtor.construtor.*;
import projeto.recurso.Consumidor;
import projeto.recurso.EntradaSaida;
import projeto.utilidade.Arquivo;
import projeto.utilidade.Json;
import projeto.utilidade.Xml;

import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public Canvas canvasDesenho;
    public Canvas canvasMapeamento;
    public Canvas canvasSelecionado;
    public ColorPicker cores;

    private Construtor construtor;
    private Construtor construtorNulo;
    private Collection<Consumidor<Objeto>> consumidores;
    private Figura figura;

    public Controller() {
        construtorNulo = new ConstrutorNulo();
        construtor = construtorNulo;
        figura = new Figura();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        consumidores = Collections
                .singletonList(
                        new ConsumidorDesenho(canvasDesenho)
                                .adicionarConsumidor(new ConsumidorEspelho(canvasMapeamento, canvasDesenho))
                );
    }

    public void eventoSelecionar() {

        System.out.println("Modo Selecionar");

        construtor = new ClippingConstrutor()
                .adicionarConsumidor(new ConsumidorDesenho(canvasDesenho));

    }

    public void eventoClipar() {

        System.out.println("Modo Clip");

        if (construtor instanceof ClippingConstrutor) {

            Retangulo retangulo = ((ClippingConstrutor) construtor).obterRetangulo();

            figura.retornarEstadoAnterior();

            new FiguraProdutor()
                    .limparTela(Arrays.asList(canvasDesenho, canvasSelecionado))
                    .adicionarConsumidor(new ConsumidorDesenho(canvasDesenho))
                    .adicionarConsumidor(new ConsumidorClipping(canvasSelecionado, canvasDesenho, retangulo))
                    .produzir(figura);

            construtor = new ConstrutorNulo();

        }
    }

    public void eventoLimpar() {

        System.out.println("Modo Limpar");

        construtor = construtorNulo;

        figura.limparFigura();

        new FiguraProdutor()
                .limparTela(Arrays.asList(canvasDesenho, canvasMapeamento, canvasSelecionado))
                .adicionarConsumidor(consumidores)
                .produzir(figura);

    }

    public void eventoSalvar() {

        System.out.println("Modo salvar");

        final EntradaSaida<Container> arquivo = new Arquivo("saida.json")
                .adicionarSerializacao(Json.obterInstancia());

        arquivo.salvarObjeto(new Container(figura));

    }

    public void eventoVoltar() {

        System.out.println("Modo Voltar");

        figura.retornarEstadoAnterior();

        new FiguraProdutor()
                .limparTela(Arrays.asList(canvasDesenho, canvasMapeamento))
                .adicionarConsumidor(consumidores)
                .produzir(figura);

    }

    public void eventoPonto() {
        System.out.println("Modo Ponto");
        construtor = new PontoConstrutor().adicionarConsumidor(consumidores);
    }

    public void eventoCirculo() {
        System.out.println("Modo Circulo");
        construtor = new CirculoConstrutor().adicionarConsumidor(consumidores);
    }

    public void eventoRetangulo() {
        System.out.println("Modo Retangulo");
        construtor = new RetanguloConstrutor().adicionarConsumidor(consumidores);
    }

    public void eventoReta() {
        System.out.println("Modo Reta");
        construtor = new RetaConstrutor().adicionarConsumidor(consumidores);
    }

    public void eventoTriangulo() {
        System.out.println("Modo Triangulo");
        construtor = new TrianguloConstrutor().adicionarConsumidor(consumidores);
    }

    public void eventoPoligono() {
        System.out.println("Modo Poligono");
        construtor = new PoligonoConstrutor().adicionarConsumidor(consumidores);
    }

    public void eventoAbrir() {

        System.out.println("Modo Abrir");

        final EntradaSaida<Container> arquivo = new Arquivo("saida.json")
                .adicionarDeserializacao(Json.obterInstancia());

        figura = arquivo
                .obterObjeto()
                .getFigura();

        new FiguraProdutor()
                .adicionarConsumidor(consumidores)
                .produzir(figura);

    }

    public void eventoDesenhar(MouseEvent mouseEvent) {

        System.out.println("desenhando em tela");

        final Color color = cores.getValue();

        final Cor cor = new Cor(color);

        final Ponto ponto = new Ponto(mouseEvent.getX(), mouseEvent.getY(), cor);

        construtor = construtor
                .processar(ponto, figura)
                .reconstruir();

    }


}
