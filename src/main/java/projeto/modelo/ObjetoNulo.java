package projeto.modelo;

import java.util.List;

public class ObjetoNulo implements Objeto {

    @Override
    public List<Ponto> construir() {
        return List.of();
    }

}
