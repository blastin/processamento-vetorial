package projeto.modelo;

import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Retangulo implements Objeto {

    @SerializedName("p1")
    private Ponto a;

    @SerializedName("p2")
    private Ponto b;

    private Cor cor;

    public Retangulo(Ponto a, Ponto b, Cor cor) {
        this.a = a;
        this.b = b;
        this.cor = cor;
    }

    private Retangulo() {
    }

    @Override
    public List<Ponto> construir() {

        var d = new Ponto(a.getX(), b.getY(), cor);
        var c = new Ponto(b.getX(), a.getY(), cor);

        var retaAD = new Reta(a, d, cor);
        var retaAC = new Reta(a, c, cor);
        var retaDB = new Reta(d, b, cor);
        var retaCB = new Reta(c, b, cor);

        return Stream
                .of(
                        retaAD.construir(),
                        retaAC.construir(),
                        retaDB.construir(),
                        retaCB.construir())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

    }

    public void setA(Ponto a) {
        this.a = a;
    }

    public void setB(Ponto b) {
        this.b = b;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

    public boolean pontoInterno(Ponto ponto) {

        Ponto maior = a.obterPontoMaior(b);
        Ponto menor = a.obterPontoMenor(b);

        return ponto.maior(menor) && ponto.menor(maior);

    }
}
