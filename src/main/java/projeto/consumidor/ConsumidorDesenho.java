package projeto.consumidor;

import javafx.scene.canvas.Canvas;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.recurso.Consumidor;
import projeto.recurso.Produtor;
import projeto.utilidade.Grafico;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ConsumidorDesenho implements Consumidor<Objeto>, Produtor<ConsumidorDesenho, List<Ponto>> {

    private final Canvas canvas;

    private final Grafico grafico;

    private List<Consumidor<List<Ponto>>> consumidores;

    public ConsumidorDesenho(Canvas canvas) {
        this.canvas = canvas;
        this.grafico = Grafico.obterInstancia();
        consumidores = new ArrayList<>();
    }

    @Override
    public void consumir(Objeto objeto) {

        List<Ponto> pontos = objeto.construir();

        pontos.forEach(ponto -> grafico.desenharPontoEmCanvas(canvas, ponto));

        produzir(pontos);

    }

    @Override
    public void produzir(List<Ponto> pontos) {
        consumidores.forEach(consumidores -> consumidores.consumir(pontos));
    }

    @Override
    public ConsumidorDesenho adicionarConsumidor(Collection<Consumidor<List<Ponto>>> consumidores) {
        this.consumidores.addAll(consumidores);
        return this;
    }

    @Override
    public ConsumidorDesenho adicionarConsumidor(Consumidor<List<Ponto>> consumidor) {
        consumidores.add(consumidor);
        return this;
    }

}
