package projeto.modelo;

import com.google.gson.annotations.SerializedName;
import projeto.recurso.IFigura;
import projeto.utilidade.Pilha;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Figura implements IFigura, Objeto {

    @SerializedName("circulo")
    private List<Circulo> circulos;

    @SerializedName("reta")
    private List<Reta> retas;

    @SerializedName("triangulo")
    private List<Triangulo> triangulos;

    @SerializedName("poligono")
    private List<Poligono> poligonos;

    @SerializedName("retangulo")
    private List<Retangulo> retangulos;

    private transient Pilha objetos;

    public Figura() {
        circulos = new ArrayList<>();
        retas = new ArrayList<>();
        triangulos = new ArrayList<>();
        poligonos = new ArrayList<>();
        retangulos = new ArrayList<>();
        objetos = new Pilha();
    }

    @Override
    public List<Ponto> construir() {

        return Stream
                .of(circulos, retas, triangulos, poligonos, retangulos)
                .flatMap(Collection::stream)
                .map(Objeto::construir)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

    }

    @Override
    public void inserirCirculo(Circulo circulo) {
        objetos.adicionarObjeto(circulo);
        circulos.add(circulo);
    }

    @Override
    public void inserirReta(Reta reta) {
        objetos.adicionarObjeto(reta);
        retas.add(reta);
    }

    @Override
    public void inserirTriangulo(Triangulo triangulo) {
        objetos.adicionarObjeto(triangulo);
        triangulos.add(triangulo);
    }

    @Override
    public void inserirRetangulo(Retangulo retangulo) {
        objetos.adicionarObjeto(retangulo);
        retangulos.add(retangulo);
    }

    @Override
    public void inserirPoligono(Poligono poligono) {
        objetos.adicionarObjeto(poligono);
        poligonos.add(poligono);
    }

    @Override
    public void retornarEstadoAnterior() {

        Optional<Objeto> objetoOptional = objetos.removerObjetoMaisRecente();

        objetoOptional.ifPresent(objeto -> {
            if (objeto instanceof Circulo) {
                circulos.remove(objeto);
            } else if (objeto instanceof Reta) {
                retas.remove(objeto);
            } else if (objeto instanceof Triangulo) {
                triangulos.remove(objeto);
            } else if (objeto instanceof Poligono) {
                poligonos.remove(objeto);
            } else if (objeto instanceof Retangulo) {
                retangulos.remove(objeto);
            }
        });
    }

    @Override
    public void limparFigura() {
        circulos.clear();
        retas.clear();
        triangulos.clear();
        poligonos.clear();
        retangulos.clear();
    }

    public void setCirculos(List<Circulo> circulos) {
        this.circulos = Optional.ofNullable(circulos).orElse(new ArrayList<>());
    }

    public void setRetas(List<Reta> retas) {
        this.retas = Optional.ofNullable(retas).orElse(new ArrayList<>());
    }

    public void setTriangulos(List<Triangulo> triangulos) {
        this.triangulos = Optional.ofNullable(triangulos).orElse(new ArrayList<>());
    }

    public void setPoligonos(List<Poligono> poligonos) {
        this.poligonos = Optional.ofNullable(poligonos).orElse(new ArrayList<>());
    }

    public void setRetangulos(List<Retangulo> retangulos) {
        this.retangulos = Optional.ofNullable(retangulos).orElse(new ArrayList<>());
    }

    private void inserirObjetoEmPilha(Objeto objeto) {
        objetos.adicionarObjeto(objeto);
    }

    Figura construirPilha() {
        if (objetos.vazio()) {
            this.circulos.forEach(this::inserirObjetoEmPilha);
            this.retas.forEach(this::inserirObjetoEmPilha);
            this.triangulos.forEach(this::inserirObjetoEmPilha);
            this.poligonos.forEach(this::inserirObjetoEmPilha);
            this.retangulos.forEach(this::inserirObjetoEmPilha);
        }
        return this;
    }
}
