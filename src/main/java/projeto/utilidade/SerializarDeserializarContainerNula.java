package projeto.utilidade;

import projeto.modelo.Container;
import projeto.recurso.Deserializar;
import projeto.recurso.Serializar;

public class SerializarDeserializarContainerNula implements Serializar<Container>, Deserializar<Container> {

    @Override
    public Container stringParaObjeto(String string) {
        return new Container();
    }

    @Override
    public String objetoParaString(Container container) {
        return "{\n" +
                "  \"circulo\": [],\n" +
                "  \"reta\": [],\n" +
                "  \"triangulo\": [],\n" +
                "  \"poligono\": [],\n" +
                "  \"retangulo\": []\n" +
                "}";
    }
}
