package projeto.utilidade;

import projeto.modelo.Objeto;
import projeto.modelo.ObjetoNulo;
import projeto.recurso.Mapeamento;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Stack;

public class Pilha implements Mapeamento {

    private final Stack<Objeto> pilhaDeObjects;

    public Pilha() {
        this.pilhaDeObjects = new Stack<>();
    }

    @Override
    public void adicionarObjeto(Objeto objeto) {
        pilhaDeObjects.push(objeto);
    }

    @Override
    public Optional<Objeto> removerObjetoMaisRecente() {
        return pilhaDeObjects.empty() ? Optional.empty() : Optional.of(pilhaDeObjects.pop());
    }

    @Override
    public List<Objeto> obterListaDeObjetos() {
        return new ArrayList<>(pilhaDeObjects);
    }

    @Override
    public List<Objeto> removerObjetos() {

        final List<Objeto> objetos = obterListaDeObjetos();

        pilhaDeObjects.clear();

        return objetos;

    }

    @Override
    public boolean vazio() {
        return pilhaDeObjects.empty();
    }

}
