package projeto.utilidade;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import projeto.modelo.Container;
import projeto.recurso.Deserializar;
import projeto.recurso.Serializar;

public final class Json implements Serializar<Container>, Deserializar<Container> {

    private Gson gson;

    private static Json INSTANCIA;

    private Json() {
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public static synchronized Json obterInstancia() {
        if (INSTANCIA == null) {
            INSTANCIA = new Json();
        }
        return INSTANCIA;
    }

    @Override
    public String objetoParaString(Container t) {
        return gson.toJson(t);
    }

    @Override
    public Container stringParaObjeto(String string) {
        return gson.fromJson(string, Container.class);
    }

}
