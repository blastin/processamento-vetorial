package projeto.produtor.construtor;

import projeto.modelo.Circulo;
import projeto.modelo.Cor;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class CirculoConstrutor extends Construtor {

    private Ponto a;
    private Double raio;
    private Cor cor;
    private IFigura figura;

    @Override
    protected boolean objetoCompleto() {
        return a != null && raio != null;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        final Circulo circulo = new Circulo(a, raio, cor);
        figura.inserirCirculo(circulo);
        return circulo;
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        if (a == null) {
            a = ponto;
        } else {
            raio = ponto.obterModuloEmRelacaoAoEixoX(a);
        }
        this.cor = ponto.getCor();
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
        this.figura = figura;
    }

    @Override
    protected Construtor renovarInstancia() {
        return new CirculoConstrutor();
    }

}
