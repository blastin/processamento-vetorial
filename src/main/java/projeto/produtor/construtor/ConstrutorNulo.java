package projeto.produtor.construtor;

import projeto.modelo.Objeto;
import projeto.modelo.ObjetoNulo;
import projeto.modelo.Ponto;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class ConstrutorNulo extends Construtor {

    @Override
    protected boolean objetoCompleto() {
        return false;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        return new ObjetoNulo();
    }

    @Override
    public void adicionarPonto(Ponto ponto) {

    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {

    }

    @Override
    protected Construtor renovarInstancia() {
        return this;
    }

}
