package projeto.utilidade;

import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import projeto.modelo.Cor;
import projeto.modelo.Ponto;

import java.util.Collections;
import java.util.List;

public final class Grafico {

    private static Grafico INSTANCIA;

    private Grafico() {
    }

    public static synchronized Grafico obterInstancia() {
        if (INSTANCIA == null) {
            INSTANCIA = new Grafico();
        }
        return INSTANCIA;
    }

    public void limnparCanvas(Canvas canva) {

        limnparCanvas(Collections.singletonList(canva));

    }

    public void limnparCanvas(List<Canvas> canvas) {

        canvas.forEach(canva -> canva.getGraphicsContext2D()
                .clearRect(0, 0, canva.getWidth(), canva.getHeight()));

    }


    public void desenharPontoEmCanvas(Canvas canvas, Ponto ponto) {

        final Cor cor = ponto.getCor();

        canvas
                .getGraphicsContext2D()
                .setFill(Color.rgb(cor.getVermelho(), cor.getVerde(), cor.getAzul()));

        canvas
                .getGraphicsContext2D()
                .fillOval(ponto.getX(), ponto.getY(), 6, 6);

    }

}
