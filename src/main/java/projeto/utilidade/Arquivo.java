package projeto.utilidade;

import projeto.modelo.Container;
import projeto.recurso.Deserializar;
import projeto.recurso.EntradaSaida;
import projeto.recurso.Serializar;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Optional;

public class Arquivo implements EntradaSaida<Container> {

    private final String caminhoArquivo;

    private Serializar<Container> serializar;

    private Deserializar<Container> deserializar;

    public Arquivo(String caminhoArquivo) {
        this.caminhoArquivo = caminhoArquivo;
        serializar = new SerializarDeserializarContainerNula();
        deserializar = new SerializarDeserializarContainerNula();
    }

    @Override
    public void salvarObjeto(Container container) {

        try (FileOutputStream fileOutputStream = new FileOutputStream(caminhoArquivo)) {

            final String serializado = serializar.objetoParaString(container);

            fileOutputStream.write(serializado.getBytes());

            fileOutputStream.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Container obterObjeto() {

        Container container = null;

        try (FileInputStream fileInputStream = new FileInputStream(caminhoArquivo)) {

            final String stringStream = new String(fileInputStream.readAllBytes(), Charset.defaultCharset());

            container = deserializar.stringParaObjeto(stringStream);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return container;

    }

    @Override
    public EntradaSaida<Container> adicionarSerializacao(Serializar<Container> serializar) {

        this.serializar = Optional
                .ofNullable(serializar)
                .orElse(new SerializarDeserializarContainerNula());

        return this;

    }

    @Override
    public EntradaSaida<Container> adicionarDeserializacao(Deserializar<Container> deserializar) {

        this.deserializar = Optional
                .ofNullable(deserializar)
                .orElse(new SerializarDeserializarContainerNula());

        return this;

    }
    
}
