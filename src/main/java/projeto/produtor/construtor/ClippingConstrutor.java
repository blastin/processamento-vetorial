package projeto.produtor.construtor;

import projeto.modelo.Cor;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.modelo.Retangulo;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class ClippingConstrutor extends Construtor {

    private Ponto a;
    private Ponto b;
    private Cor cor;
    private IFigura figura;

    public ClippingConstrutor() {
        cor = Cor.COR_PADRAO;
    }

    @Override
    protected boolean objetoCompleto() {
        return a != null && b != null;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        Retangulo retangulo = obterRetangulo();
        figura.inserirRetangulo(retangulo);
        return retangulo;
    }

    public Retangulo obterRetangulo() {
        return new Retangulo(a, b, cor);
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        if (a == null) {
            a = ponto;
        } else {
            b = ponto;
        }
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
        this.figura = figura;
    }

    @Override
    protected Construtor renovarInstancia() {
        return this;
    }

}
