package projeto.produtor;

import javafx.scene.canvas.Canvas;
import projeto.consumidor.ConsumidorNulo;
import projeto.modelo.Objeto;
import projeto.recurso.Consumidor;
import projeto.recurso.Produtor;
import projeto.utilidade.Grafico;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class FiguraProdutor implements Produtor<FiguraProdutor, Objeto> {

    private Collection<Consumidor<Objeto>> consumidores;

    public FiguraProdutor() {
        consumidores = new ArrayList<>();
    }

    public final FiguraProdutor limparTela(List<Canvas> canvas) {
        Grafico.obterInstancia().limnparCanvas(canvas);
        return this;
    }

    @Override
    public void produzir(Objeto t) {
        consumidores.forEach(consumidor -> consumidor.consumir(t));
    }

    @Override
    public FiguraProdutor adicionarConsumidor(Collection<Consumidor<Objeto>> consumidores) {
        this.consumidores.addAll(consumidores);
        return this;
    }

    @Override
    public FiguraProdutor adicionarConsumidor(Consumidor<Objeto> consumidor) {
        consumidores.add(Optional.ofNullable(consumidor).orElse(ConsumidorNulo.obterInstancia()));
        return this;
    }

}
