package projeto.modelo;

import java.util.List;

public interface Objeto {

    List<Ponto> construir();

}
