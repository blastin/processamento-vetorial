package projeto.produtor;

import projeto.consumidor.ConsumidorNulo;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.recurso.Consumidor;
import projeto.recurso.IFigura;
import projeto.recurso.Produtor;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;

public abstract class Construtor implements Produtor<Construtor, Objeto> {

    private Collection<Consumidor<Objeto>> consumidores;

    public Construtor() {
        consumidores = new LinkedHashSet<>();
    }

    public final Construtor processar(Ponto ponto, IFigura figura) {

        adicionarPonto(ponto);

        if (objetoCompleto()) {

            adicionarInstanciaFigura(figura);

            final Objeto objeto = obterObjetoParaConstrucao();

            produzir(objeto);

        }

        return this;

    }

    public final Construtor reconstruir() {

        if (objetoCompleto()) {

            return renovarInstancia()
                    .adicionarConsumidor(consumidores);

        }

        return this;

    }

    @Override
    public void produzir(Objeto t) {
        consumidores.forEach(consumidor -> consumidor.consumir(t));
    }

    @Override
    public Construtor adicionarConsumidor(Collection<Consumidor<Objeto>> consumidores) {

        this.consumidores.addAll(consumidores);

        return this;
    }

    @Override
    public Construtor adicionarConsumidor(Consumidor<Objeto> consumidor) {

        consumidores.add(Optional.ofNullable(consumidor).orElse(ConsumidorNulo.obterInstancia()));

        return this;
    }

    protected abstract boolean objetoCompleto();

    protected abstract Objeto obterObjetoParaConstrucao();

    public abstract void adicionarPonto(Ponto ponto);

    protected abstract void adicionarInstanciaFigura(IFigura figura);

    protected abstract Construtor renovarInstancia();

}
