package projeto.modelo;

import java.util.List;
import java.util.Optional;

public final class Ponto implements Objeto {

    private Double x;

    private Double y;

    private transient Cor cor;

    public Ponto(Double x, Double y, Cor cor) {
        this.x = x;
        this.y = y;
        this.cor = cor;
    }

    private Ponto() {
    }

    @Override
    public List<Ponto> construir() {
        return List.of(this);
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Cor getCor() {
        return Optional.ofNullable(cor).orElse(Cor.COR_PADRAO);
    }

    public Double obterModuloEmRelacaoAoEixoX(Ponto ponto) {
        return Math.abs(ponto.x - x);
    }

    public void setX(Double x) {
        this.x = x;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

    boolean maior(Ponto ponto) {
        return x.compareTo(ponto.getX()) >= 0 && y.compareTo(ponto.getY()) >= 0;
    }

    boolean menor(Ponto ponto) {
        return x.compareTo(ponto.getX()) < 0 && y.compareTo(ponto.getY()) < 0;
    }

    Ponto obterPontoMaior(Ponto ponto) {
        return maior(ponto) ? this : ponto;
    }

    Ponto obterPontoMenor(Ponto ponto) {
        return menor(ponto) ? this : ponto;
    }

}
