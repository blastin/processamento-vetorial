package projeto.modelo;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Triangulo implements Objeto {

    private Ponto a;

    private Ponto b;

    private Ponto c;

    private Cor cor;

    public Triangulo(Ponto a, Ponto b, Ponto c, Cor cor) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.cor = cor;
    }

    private Triangulo(){ }

    @Override
    public List<Ponto> construir() {

        var retaAB = new Reta(a, b, cor);

        var retaAC = new Reta(a, c, cor);

        var retaBC = new Reta(b, c, cor);

        return Stream
                .of(
                        retaAB.construir(),
                        retaAC.construir(),
                        retaBC.construir())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

    }

    public void setA(Ponto a) {
        this.a = a;
    }

    public void setB(Ponto b) {
        this.b = b;
    }

    public void setC(Ponto c) {
        this.c = c;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }
}
