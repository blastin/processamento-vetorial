package projeto.produtor.construtor;

import projeto.modelo.Cor;
import projeto.modelo.Objeto;
import projeto.modelo.Ponto;
import projeto.modelo.Triangulo;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

public class TrianguloConstrutor extends Construtor {

    private Ponto a;
    private Ponto b;
    private Ponto c;
    private IFigura figura;
    private Cor cor;

    @Override
    protected boolean objetoCompleto() {
        return a != null && b != null && c != null;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        Triangulo triangulo = new Triangulo(a, b, c, cor);
        figura.inserirTriangulo(triangulo);
        return triangulo;
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        if (a == null) {
            a = ponto;
        } else if (b == null) {
            b = ponto;
        } else {
            c = ponto;
        }
        cor = ponto.getCor();
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
        this.figura = figura;
    }

    @Override
    protected Construtor renovarInstancia() {
        return new TrianguloConstrutor();
    }

}
