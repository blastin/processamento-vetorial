package projeto.produtor.construtor;

import projeto.modelo.*;
import projeto.produtor.Construtor;
import projeto.recurso.IFigura;

import java.util.ArrayList;
import java.util.List;

public class PoligonoConstrutor extends Construtor {

    private List<Ponto> pontos;

    private IFigura figura;

    private Poligono poligono;

    private Cor cor;

    public PoligonoConstrutor() {
        pontos = new ArrayList<>();
    }

    @Override
    protected boolean objetoCompleto() {
        return pontos.size() > 1;
    }

    @Override
    protected Objeto obterObjetoParaConstrucao() {
        if (poligono == null) {
            poligono = new Poligono(pontos, cor);
            figura.inserirPoligono(poligono);
        }
        return new Reta(pontos.get(pontos.size() - 2), pontos.get(pontos.size() - 1), cor);
    }

    @Override
    public void adicionarPonto(Ponto ponto) {
        pontos.add(ponto);
        this.cor = ponto.getCor();
    }

    @Override
    protected void adicionarInstanciaFigura(IFigura figura) {
        this.figura = figura;
    }

    @Override
    protected Construtor renovarInstancia() {
        return this;
    }
}
