package projeto.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Container {

    private Figura figura;

    public Container(Figura figura) {
        this.figura = figura;
    }

    public Container() {
    }

    public Figura getFigura() {
        return figura.construirPilha();
    }

}
