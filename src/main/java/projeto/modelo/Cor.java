package projeto.modelo;

import com.google.gson.annotations.SerializedName;
import javafx.scene.paint.Color;

public class Cor {

    public static final Cor COR_PADRAO = new Cor(Color.BLACK);

    @SerializedName("r")
    private Integer vermelho;

    @SerializedName("g")
    private Integer verde;

    @SerializedName("b")
    private Integer azul;

    public Cor(Color value) {
        vermelho = Double.valueOf(value.getRed() * 255).intValue();
        verde = Double.valueOf(value.getGreen() * 255).intValue();
        azul = Double.valueOf(value.getBlue() * 255).intValue();
    }

    private Cor(){ }

    public Integer getVermelho() {
        return vermelho;
    }

    public Integer getVerde() {
        return verde;
    }

    public Integer getAzul() {
        return azul;
    }

    public void setVermelho(Integer vermelho) {
        this.vermelho = vermelho;
    }

    public void setVerde(Integer verde) {
        this.verde = verde;
    }

    public void setAzul(Integer azul) {
        this.azul = azul;
    }
}
