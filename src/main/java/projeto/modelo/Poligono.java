package projeto.modelo;

import java.util.ArrayList;
import java.util.List;

public class Poligono implements Objeto {

    private List<Ponto> pontos;

    private Cor cor;

    private Poligono() {
    }

    public Poligono(List<Ponto> pontos, Cor cor) {
        this.pontos = pontos;
        this.cor = cor;
    }

    @Override
    public List<Ponto> construir() {

        List<Ponto> pontosConstruidos = new ArrayList<>();

        var tamanho = pontos.size() - 2;

        while (tamanho >= 0) {

            var a = pontos.get(tamanho);
            var b = pontos.get(tamanho + 1);

            var reta = new Reta(a, b, cor);

            tamanho -= 1;

            pontosConstruidos.addAll(reta.construir());

        }

        return pontosConstruidos;

    }

    public void setPontos(List<Ponto> pontos) {
        this.pontos = pontos;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

}
