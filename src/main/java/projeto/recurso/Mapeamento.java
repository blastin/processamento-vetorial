package projeto.recurso;

import projeto.modelo.Objeto;

import java.util.List;
import java.util.Optional;

public interface Mapeamento {

    void adicionarObjeto(Objeto objeto);

    Optional<Objeto> removerObjetoMaisRecente();

    List<Objeto> obterListaDeObjetos();

    List<Objeto> removerObjetos();

    boolean vazio();

}
