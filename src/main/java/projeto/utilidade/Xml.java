package projeto.utilidade;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import projeto.modelo.Container;
import projeto.recurso.Deserializar;
import projeto.recurso.Serializar;

public final class Xml implements Serializar<Container>, Deserializar<Container> {

    private static Xml INSTANCIA;

    private XmlMapper xmlMapper;

    private Xml() {
        xmlMapper = new XmlMapper();
        xmlMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    public static synchronized Xml obterInstancia() {
        if (INSTANCIA == null) {
            INSTANCIA = new Xml();
        }
        return INSTANCIA;
    }

    @Override
    public Container stringParaObjeto(String string) {
        try {
            return xmlMapper.readValue(string, Container.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new Container();
    }

    @Override
    public String objetoParaString(Container container) {
        try {
            return xmlMapper.writeValueAsString(container);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }
}
