package projeto.recurso;

public interface Serializar<T> {

    String objetoParaString(T t);

}
