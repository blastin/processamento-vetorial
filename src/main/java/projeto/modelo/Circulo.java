package projeto.modelo;

import java.util.ArrayList;
import java.util.List;

public class Circulo implements Objeto {

    private Ponto ponto;

    private Double raio;

    private Cor cor;

    public Circulo(Ponto ponto, Double raio, Cor cor) {
        this.ponto = ponto;
        this.raio = raio;
        this.cor = cor;
    }

    private Circulo(){ }

    @Override
    public List<Ponto> construir() {

        List<Ponto> pontos = new ArrayList<>();

        for (double taxaAngular = 0.0; taxaAngular < 360; taxaAngular += 0.1) {

            double y = raio * Math.sin(taxaAngular) + ponto.getY();

            double x = raio * Math.cos(taxaAngular) + ponto.getX();

            pontos.add(new Ponto(x, y, cor));

        }

        return pontos;

    }

    public void setPonto(Ponto ponto) {
        this.ponto = ponto;
    }

    public void setRaio(Double raio) {
        this.raio = raio;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

}
