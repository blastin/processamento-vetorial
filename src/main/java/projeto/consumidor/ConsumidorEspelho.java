package projeto.consumidor;

import javafx.scene.canvas.Canvas;
import projeto.modelo.Ponto;
import projeto.recurso.Consumidor;
import projeto.utilidade.EspelhamentoTela;
import projeto.utilidade.Grafico;

import java.util.List;

public class ConsumidorEspelho implements Consumidor<List<Ponto>> {

    private final Grafico grafico;

    private final Canvas canvasEspelho;

    private final EspelhamentoTela espelhamentoTela;

    public ConsumidorEspelho(Canvas canvasEspelho, Canvas canvasDesenho) {
        this.canvasEspelho = canvasEspelho;
        this.grafico = Grafico.obterInstancia();
        espelhamentoTela = new EspelhamentoTela(canvasDesenho, canvasEspelho);
    }

    @Override
    public void consumir(List<Ponto> pontos) {

        pontos
                .stream()
                .map(espelhamentoTela)
                .forEach(ponto -> grafico.desenharPontoEmCanvas(canvasEspelho, ponto));

    }

}
