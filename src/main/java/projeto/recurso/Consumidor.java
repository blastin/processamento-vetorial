package projeto.recurso;

public interface Consumidor<T> {

    void consumir(T t);

}
