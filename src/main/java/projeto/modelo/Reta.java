package projeto.modelo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Reta implements Objeto {

    @SerializedName("p1")
    private Ponto a;

    @SerializedName("p2")
    private Ponto b;

    private Cor cor;

    public Reta(Ponto a, Ponto b, Cor cor) {
        this.a = a;
        this.b = b;
        this.cor = cor;
    }

    private Reta(){ }

    @Override
    public List<Ponto> construir() {

        List<Ponto> pontos = new ArrayList<>();

        var x1 = a.getX();
        var x2 = b.getX();

        var y1 = a.getY();
        var y2 = b.getY();

        var dy = Math.abs(y2 - y1);
        var dx = Math.abs(x2 - x1);
        var x = x1;
        var y = y1;

        var steps = Math.max(Math.abs(dy), Math.abs(dx));

        var xIncrement = dx / steps;

        var yIncrement = dy / steps;

        if (y1 > y2) {
            yIncrement *= -1;
        }
        if (x1 > x2) {
            xIncrement *= -1;
        }

        pontos.add(new Ponto(x, y, cor));

        for (var k = 0.0; k < steps; k++) {

            x += xIncrement;

            y += yIncrement;

            pontos.add(new Ponto(x, y, cor));

        }

        return pontos;

    }

    public void setA(Ponto a) {
        this.a = a;
    }

    public void setB(Ponto b) {
        this.b = b;
    }

    public void setCor(Cor cor) {
        this.cor = cor;
    }

}
